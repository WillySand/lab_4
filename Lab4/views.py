#from django.shortcuts import render
#from django.http import HttpResponse


#def index(request):
#    return render(request, 'index_Lab4.html')
from django.views.generic import TemplateView

class HomePage(TemplateView):
    template_name = 'index_Lab4.html'


class PageTwo(TemplateView):
    template_name = 'page2.html'
