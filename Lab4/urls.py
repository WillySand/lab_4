from django.urls import path

from .views import HomePage, PageTwo

urlpatterns = [
    path('about/', PageTwo.as_view(), name='about'),
    path('', HomePage.as_view(), name='home'),  
]
